# Fonctionnalités pour une page d'inscription

## Le formulaire

Réaliser une page web contenant un formulaire

Ce formulaire doit contenir les champs suivants:

- Firstname ( type text )
- Lastname ( type text )
- Email ( type email )
- Password1 (type password )
- Password2 ( type password) ( retapez le mot de passe pour être sur )

- Numéro de rue ( type text )
- Nom de la ville ( type text )
- Code postal ( type number )
- Nom de la rue ( type text )

- Date de naissance (type date )
- J'accepte les conditions ( case à cocher )

### Controles - Critère de réussite

- Firstname et lastname contiennent juste des lettres.
- Email est de la forme: "user@domaine.net"
- Password1 et Password2 doivent être identique
- Password1 doit contenir des chiffres & des lettres & Caractère spéciaux
- Code postal est un nombre allant de 0 à 99999
- Ville est du texte
- Numéro de rue peut contenir des chiffres & des lettres uniquement

- Date de naissance est une date plus vieille que la date du jour
- La case "J'accepte les conditions" doit être cochée.

## Confirmation d'inscription

### L'envoi de l'email de confirmation

Une fois inscrit, on envoie un email de confirmation à l'adresse EMAIL du client  collecté
dans le formulaire précédent.

Ce mail doit :
- Avoir comme titre de mail "Boulangerie Marie-Blachere - Confirmation d'inscription"
- Avoir comme contenu:
    "
    Bonjour $prenom !
  
    Merci pour ton inscription sur notre site, afin de confirmer ton compte, merci de cliqueez
    sur le lien: http://LIEN/confirm_identifiant?
"
- Avoir comme destinaire: l'email du client.

Attention, le lien doit avoir une durée de validité de 2H par exemple.

#### Lorsque le client clique sur le lien

Lorsque le client clique sur le lien, on active son compte, et ensuite,
il arrive sur une page lui informant que son compte est actif.

## Sécurisation des accès

Lorsque l'utilisateur ayant un compte non-actif, il est automatiquement redirigé vers une page

contenant:

"Votre compte n'est pas activé, veuillez cliquez sur le lien reçu par mail."
